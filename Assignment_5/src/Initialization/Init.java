/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import Business.City;
import Business.Community;
import Business.Family;
import Business.House;
import Business.Patient;
import Business.Person;
import Business.VitalSign;
import Business.VitalSignHistory;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author rahulchandra
 */
public class Init {

    private City city;

    public Init() {
        city = new City("Boston");
        addCommunities();
        addHouses();
        addFamilies();
        addPerson();

    }
// To add communities

    public void addCommunities() {
        Random rand = new Random();
        String characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        for (int i = 0; i < 10; i++) {
            Community community = city.getCommunityDirectory().addCommunity("Community_" + generateString(rand, characters, 7));
            //System.out.println(community.getAlphaId()+"    "+community.getCommunityName());
        }
    }
// To add Houses

    public void addHouses() {
        Random rand = new Random();
        String characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (int i = 0; i < 20; i++) {
                House house = community.getHouseDirectory().addHouse("House_" + generateString(rand, characters, 5));
                //System.out.println(house.getAlphaId() +"    "+house.getHouseName());
            }
        }
    }
// To add families

    public void addFamilies() {
        Random rand = new Random();
        String characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                int n = randomFunc();

                for (int i = 0; i < n; i++) {
                    Family family = house.getFamilyDirectory().addFamily("Family_" + generateString(rand, characters, 6));
                    //System.out.println(house.getAlphaId() + " " + family.getAlphaId() + "    " + family.getFamilyName());

                }
            }
        }
    }

// To add Persons
    public void addPerson() {

        Random rand = new Random();
        String characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    int n = 3 + memberToggleFunc();
                    for (int i = 0; i < n; i++) {

                        String data[] = getPersonRandom(i);
                        Person person = family.getPersonDirectory().addPerson("Name" + generateString(rand, characters, 6), data[0], data[1], null, null, null);
                        Patient patient = person.getPatient();
                        VitalSignHistory vitalSignHistory = patient.getVitalSignHistory();
                        for (int k = 0; k < 5; k++) {
                            vitalSignHistory.addVitalSign(randomInRange(3.6F, 10.3F), randomInRange(0.8F, 4.0F), rand.nextInt(110) + 90, boolToggle(), boolToggle(), boolToggle());
                        }
                        vitalSignHistory.averageVitals(person);
                        // System.out.println(v.getDate()+ " Total chol  "+ v.getTotalCholesterol() +" HDL" +v.getHdlCholesterol() +" BP "+ v.getSystolicBloodPressure()+" hasBP"+ v.isHasBloodPressure()+ "  Smoker"+v.isIsSmoker()+ " diabetic"+ v.isIsDiabetic());
                    }
                    ArrayList<Person> personList = family.getPersonDirectory().getPersonDirectory();
                    for (int i = 0; i < personList.size(); i++) {
                        switch (personList.size()) {
                            case 3:
                                if (i == 0 || i == 1) {
                                    personList.get(i).setFather(null);
                                    personList.get(i).setMother(null);
                                    personList.get(i).setSiblings(null);
                                } else {
                                    personList.get(i).setFather(personList.get(0));
                                    personList.get(i).setMother(personList.get(1));
                                    personList.get(i).setSiblings(null);
                                }
                                break;
                            case 4:
                                if (i == 0 || i == 1) {
                                    personList.get(i).setFather(null);
                                    personList.get(i).setMother(null);
                                    personList.get(i).setSiblings(null);
                                } else if (i == 2 || i == 3) {
                                    personList.get(i).setFather(personList.get(0));
                                    personList.get(i).setMother(personList.get(1));
                                    Person sibling = (i == 2) ? personList.get(i + 1) : personList.get(i - 1);
                                    personList.get(i).setSiblings(sibling);
                                }
                                break;
                            case 5:
                                if (i == 0 || i == 1) {
                                    Person father = (i == 0) ? personList.get(personList.size() - 1) : null;
                                    personList.get(i).setFather(father);
                                    personList.get(i).setMother(null);
                                    personList.get(i).setSiblings(null);
                                } else if (i == 2 || i == 3) {
                                    personList.get(i).setFather(personList.get(0));
                                    personList.get(i).setMother(personList.get(1));
                                    Person sibling = (i == 2) ? personList.get(i + 1) : personList.get(i - 1);
                                    personList.get(i).setSiblings(sibling);
                                } else if (i == 4) {
                                    personList.get(i).setFather(null);
                                    personList.get(i).setMother(null);
                                    personList.get(i).setSiblings(null);
                                }
                                break;
                            case 6:
                                if (i == 0 || i == 1) {
                                    Person father = (i == 0) ? personList.get(personList.size() - 2) : null;
                                    personList.get(i).setFather(father);
                                    Person mother = (i == 0) ? personList.get(personList.size() - 1) : null;
                                    personList.get(i).setMother(mother);
                                    personList.get(i).setSiblings(null);
                                } else if (i == 2 || i == 3) {
                                    personList.get(i).setFather(personList.get(0));
                                    personList.get(i).setMother(personList.get(1));
                                    Person sibling = (i == 2) ? personList.get(i + 1) : personList.get(i - 1);
                                    personList.get(i).setSiblings(sibling);
                                } else if (i == 4 || i == 5) {
                                    personList.get(i).setFather(null);
                                    personList.get(i).setMother(null);
                                    personList.get(i).setSiblings(null);
                                }
                                break;
                        }
                    }
                }
            }
        }

    }

//To generate age and gender
    public String[] getPersonRandom(int index) {
        int age = 0;
        String gender = "";
        String data[] = new String[2];
        Random r = new Random();
        if (index == 0) {
            age = r.nextInt((60 - 41) + 1) + 41;
            gender = "male";
        } else if (index == 1) {
            age = r.nextInt((60 - 41) + 1) + 41;
            gender = "female";
        }
        if (index == 2) {
            age = r.nextInt((40 - 31) + 1) + 31;
            gender = "male";
        } else if (index == 3) {
            age = r.nextInt((40 - 31) + 1) + 31;
            gender = "female";
        } else if (index == 4) {
            age = r.nextInt((100 - 61) + 1) + 61;
            gender = "male";
        } else if (index == 5) {
            age = r.nextInt((100 - 61) + 1) + 61;
            gender = "female";
        }
        data[0] = String.valueOf(age);
        data[1] = gender;
        return data;
    }

//To Generate Random Values
    public int randomFunc() {
        final Random randomGenerator = new Random();
        int chance = randomGenerator.nextInt(10);
        return chance > 6 ? 2 : 1;
    }

    public static String generateString(Random rng, String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

    public int memberToggleFunc() {
        final Random randomGenerator = new Random();
        int chance = randomGenerator.nextInt(4);
        return chance;
    }

    public static float randomInRange(float min, float max) {
        Random random = new Random();
        float range = max - min;
        float scale = random.nextFloat() * range;
        float shift = scale + min;
        return shift;
    }

    public boolean boolToggle() {
        Random rand = new Random();
        int chance = rand.nextInt(10);
        if (chance > 7) {
            return true;
        } else {
            return false;
        }
    }

    //To display city report
    public void cityReports() {
        System.out.println("The total population of city is:");
        city.populationCount();
        System.out.println("The number of male and female diabetic patients:");
        city.getCityMaleDiabetics();
        System.out.println("How many people in city has risk of getting heart disease?");
        city.peopleWithHeartDisease();
        System.out.println("Percent of healthy people in city");
        city.healthyPeoplepercent();
        System.out.println("Diabetic number of people in each age group ");
        city.diabeticPeopleAgeGroup();
        System.out.println("Ratio of smokers to non-smokers in city");
        city.ratioOfSmokers();
    }

        //To display community report
    public void communityReports() {
        boolean communityExist = false;
        System.out.println("Enter the community ID");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String id[] = new String[10];
        for (int i = 0; i < 10; i++) {
            id[i] = "c" + i;
        }
        for (int i = 0; i < 10; i++) {
            if (input.equals(id[i])) {
                communityExist = true;
                break;
            }
        }
        if (communityExist == true) {
            System.out.println("How many people in the community have high BP ?");
            city.getCommunityDirectory().hasHighBp(input);

            System.out.println("What percent of men and women have high cholestrol ?");
            city.getCommunityDirectory().hasHighCholestrol(input);

            System.out.println("Which age group has max Number of smokers ?");
            city.getCommunityDirectory().ageSmokerGroup(input);
        }
    }

    //To display house report

    public void houseReports() {
        System.out.println("Enter the HouseID : ");
        Scanner sc = new Scanner(System.in);
        String houseID = sc.next();
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {

            if (community.getHouseDirectory().avgPeopleChart(houseID)) {
                break;
            }
        }
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {

            if (community.getHouseDirectory().familyCheck(houseID)) {
                break;
            }
        }

    }

    
    //To display family Reports
    public void familyReports() {
        System.out.println("Please enter family id for finding diabetic to non diabetic ratio");
        Scanner sc = new Scanner(System.in);
        String id = sc.nextLine();
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                if (house.getFamilyDirectory().maxRisk(id)) {
                    break;
                }
            }
        }
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                if (house.getFamilyDirectory().checkDiabetesHereditary(id)) {
                    break;
                }
            }
        }

        
        

        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                if (house.getFamilyDirectory().highRiskPercentPerson(id)) {
                    break;
                }
            }
        }
    }

    //To display Person reports
    public void personReports() {
        boolean personExist = false;
        System.out.println("Enter the person ID");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        getPersonReport(input);
    }

    //To Get Risk score for Person
    public void getPersonReport(String id) {
        for (Community community : city.getCommunityDirectory().getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        if (person.getAlphaId().equalsIgnoreCase(id)) {
                            System.out.println("Name:" + person.getName() + "\n Risk Score:" + person.getPatient().getRiskPercent() + "\n");
                            System.out.println("Risk Score deviation from average Risk Score:" + (person.getPatient().getRiskPercent() - person.getPatient().getAverageRisk()) + "\n");
                            VitalSign meanVitalSign = person.getPatient().getVitalSignHistory().averageVitals(person);
                            System.out.println("Total Cholestrol:" + meanVitalSign.getTotalCholesterol());
                            System.out.println("HDL Cholestrol:" + meanVitalSign.getHdlCholesterol());
                            System.out.println("Systolic Blood Pressure:" + meanVitalSign.getSystolicBloodPressure());
                            System.out.println("Smoker:" + meanVitalSign.isIsSmoker());
                            System.out.println("Diabetic:" + meanVitalSign.isIsDiabetic());
                        }
                    }
                }
            }

        }
    }

}

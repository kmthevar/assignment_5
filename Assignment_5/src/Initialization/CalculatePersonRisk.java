/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import java.lang.*;
import java.util.Date;
import java.util.Scanner;
import jdk.nashorn.internal.runtime.JSType;
import Business.VitalSignHistory;
import Business.Person;
import Business.VitalSign;

/**
 *
 * @author rahulchandra
 */
public class CalculatePersonRisk {
    
    public CalculatePersonRisk() {
    
        createPerson();
    }
    
    
    public void createPerson() {
     
        String name = "";
        String age = "";
        String gender = "";
        float totalCholesterol = 0;
        float hdlCholesterol = 0;
        int systolicPressure = 0;
        boolean isSmoker = false;
        boolean isDiabetic = false;
        boolean isBloodPressure = false;
        int flag = 0;
        
        Scanner sc = new Scanner(System.in);
        

            System.out.println("Please enter your personal details:");
            System.out.println("Enter your name:");
            
            
            name = sc.nextLine();
            
            while(name.trim().matches(".*\\d+.*") || name.trim().equals("") )
            {
                System.out.println("Please enter valid name:");
                name = sc.nextLine();
            }
            
            System.out.println("Enter your age:");
            
            
            while(!sc.hasNextInt()) {
                
                System.out.println("Please enter valid age:");
                sc.next();
            }
            
           int  age2 = sc.nextInt();
            
            while(age2 < 30 || age2 > 100) {
                System.out.println("Age must be between 30 and 100:");
                while(!sc.hasNextInt()) {
                
                System.out.println("Please enter valid age:");
                sc.next();
            }
            
              age2 = sc.nextInt();
            }
                age = String.valueOf(age2);
            

            System.out.println("Please select your gender:\n1.Male 2.Female");
            
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                sc.next();

   
            }
            flag = sc.nextInt();
            
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                sc.next();

   
            }
            flag = sc.nextInt();
            }

            
            if(flag==1)
                gender = "male";
            else
                gender = "female";
            
        
        
        
        

            System.out.println("\nPlease enter your vital signs details:");
            System.out.println("Total Cholesterol:");
            
            while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of Total cholesterol");
                sc.next();
            }
            totalCholesterol = sc.nextFloat();
            
            while(totalCholesterol < 3.59 || totalCholesterol > 10.7) 
            {
                System.out.println("Total cholesterol must be between 3.6 to 10.6:");
                while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of Total cholesterol");
                sc.next();
            }
            totalCholesterol = sc.nextFloat();
            }
            
            totalCholesterol = Math.round(totalCholesterol*10);
            totalCholesterol = totalCholesterol/10;
            
            
            System.out.println("HDL Cholesterol:");
            
            while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of HDL cholesterol");
                sc.next();
            }
            hdlCholesterol = sc.nextFloat();
            
            while(hdlCholesterol < 0.79 || hdlCholesterol > 4.1) 
            {
                System.out.println("HDL cholesterol must be between 0.8 to 4.0:");
                while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of HDL cholesterol");
                sc.next();
            }
            hdlCholesterol = sc.nextFloat();
            }
            
            hdlCholesterol = Math.round(hdlCholesterol*10);
            hdlCholesterol = hdlCholesterol/10;
            
            
            
            
            System.out.println("Systolic Blood Pressure:");
            
            while (!sc.hasNextInt()) {
                System.out.println("Please enter valid value of Systolic pressure");
                sc.next();
            }
            
            systolicPressure = sc.nextInt();
            
            while(systolicPressure < 90 || systolicPressure > 200)
            {
                System.out.println("Systolic Pressure must be between 90 to 200:");
                while (!sc.hasNextInt()) {
                System.out.println("Please enter valid value of Systolic pressure");
                sc.next();
            }
            
            systolicPressure = sc.nextInt();
            }

            
            
            
            
            System.out.println("Do you smoke:\n1.Yes 2.No");
            
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();

            }
            flag = Integer.parseInt(sc.next());
            
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();

            }
            flag = Integer.parseInt(sc.next());
            }
            
            if(flag == 1)
                isSmoker = true;
            
            
            
            
            System.out.println("Diabetic:\n1.Yes 2.No");
            
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();

   
            }
            flag = sc.nextInt();
            
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();

   
            }
            flag = sc.nextInt();
            }
            
            if(flag == 1)
                isDiabetic = true;
            
            
       
        
        Person person = new Person(name, age, gender, null, null, null);
       
       VitalSign vitalSign= person.getPatient().getVitalSignHistory().addVitalSign(totalCholesterol, hdlCholesterol, systolicPressure, isSmoker, isDiabetic, isBloodPressure);
       person.getPatient().getVitalSignHistory().calculateRisk(vitalSign, person);
       System.out.println("The risk percent is:"+person.getPatient().getRiskPercent()+"%");
    
    }
    
}


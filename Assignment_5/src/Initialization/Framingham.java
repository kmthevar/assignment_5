/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Initialization;

import java.util.Scanner;

/**
 *
 * @author rahulchandra
 */
public class Framingham {

    private Init init;
    private CalculatePersonRisk calculatePersonRisk;

    public static void main(String[] args) {

        new Framingham().menuDriven();

    }

    public void menuDriven() {
        System.out.println("FRAMINGHAM CASE STUDY");
        System.out.println("1. Risk Calculator \n 2.Display reports \n 3. Break");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                calculatePersonRisk = new CalculatePersonRisk();
                System.out.println("1.Do you wish to continue ? \n 1.Yes 2.No");
                choice = sc.nextInt();
                if (choice == 1) {
                    menuDriven();
                }
                break;
            case 2:
                init = new Init();
                reportGeneration();
                if (choice == 1) {
                    menuDriven();
                }
                break;
            case 3:
                break;
        }
    }

    public void reportGeneration() {
        System.out.println("REPORTS");
        System.out.println("1. City \n 2. Community \n 3. House \n 4. Family \n 5.Person \n 6.Back");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                init.cityReports();
                reportGeneration();
                break;
            case 2:
                init.communityReports();
                reportGeneration();
                break;
            case 3:
                init.houseReports();
                reportGeneration();
                break;
            case 4:
                init.familyReports();
                reportGeneration();
                break;
            case 5:
                init.personReports();
                reportGeneration();
                break;
            case 6:
                break;
        }
    }

}

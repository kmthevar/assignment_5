/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rahulchandra
 */
public class Patient {
    
   private VitalSignHistory vitalSignHistory;
   private int riskPercent;
   private int averageRisk;
    
    public Patient() {
        vitalSignHistory = new VitalSignHistory();
    }

    /**
     * @return the vitalSignHistory
     */
    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    /**
     * @param vitalSignHistory the vitalSignHistory to set
     */
    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }

    /**
     * @return the riskPercent
     */
    public int getRiskPercent() {
        return riskPercent;
    }

    /**
     * @param riskPercent the riskPercent to set
     */
    public void setRiskPercent(int riskPercent) {
        this.riskPercent = riskPercent;
    }

    public int getAverageRisk() {
        return averageRisk;
    }

    public void setAverageRisk(int averageRisk) {
        this.averageRisk = averageRisk;
    }        

}

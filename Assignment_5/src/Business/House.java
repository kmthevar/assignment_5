/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Admin
 */
public class House {
    
    private static int houseId=0;
    private String alphaId;
    private String houseName;
    private FamilyDirectory familyDirectory;
    
    public House(String houseName){
        houseId++;
        alphaId= "H"+houseId;
        this.houseName =houseName;
        familyDirectory = new FamilyDirectory();
    }

    /**
     * @return the houseId
     */
    public int getHouseId() {
        return houseId;
    }

    /**
     * @param houseId the houseId to set
     */
    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    /**
     * @return the houseName
     */
    public String getHouseName() {
        return houseName;
    }

    /**
     * @param houseName the houseName to set
     */
    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    /**
     * @return the familyDirectory
     */
    public FamilyDirectory getFamilyDirectory() {
        return familyDirectory;
    }

    /**
     * @param familyDirectory the familyDirectory to set
     */
    public void setFamilyDirectory(FamilyDirectory familyDirectory) {
        this.familyDirectory = familyDirectory;
    }

    /**
     * @return the alphaId
     */
    public String getAlphaId() {
        return alphaId;
    }

    /**
     * @param alphaId the alphaId to set
     */
    public void setAlphaId(String alphaId) {
        this.alphaId = alphaId;
    }
    
}

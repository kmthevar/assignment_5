/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author rahulchandra
 */
public class VitalSign {
    
    private Date date;
    private float totalCholesterol;
    private float hdlCholesterol;
    private int systolicBloodPressure;
    private boolean isSmoker;
    private boolean isDiabetic;
    private boolean hasBloodPressure;
    
    public VitalSign(float totalCholesterol, float hdlCholesterol,int systolicBloodPressure, boolean isSmoker, boolean isDiabetic, boolean hasBloodPressure ) {
        date = new Date();
        this.totalCholesterol= totalCholesterol;
        this.hdlCholesterol= hdlCholesterol;
        this.systolicBloodPressure= systolicBloodPressure;
        this.isSmoker= isSmoker;
        this.isDiabetic= isDiabetic;
        this.hasBloodPressure= hasBloodPressure;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }    
    
    public float getTotalCholesterol() {
        return totalCholesterol;
    }

    public void setTotalCholesterol(float totalCholesterol) {
        this.totalCholesterol = totalCholesterol;
    }

    public float getHdlCholesterol() {
        return hdlCholesterol;
    }

    public void setHdlCholesterol(float hdlCholesterol) {
        this.hdlCholesterol = hdlCholesterol;
    }

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public boolean isIsSmoker() {
        return isSmoker;
    }

    public void setIsSmoker(boolean isSmoker) {
        this.isSmoker = isSmoker;
    }

    public boolean isIsDiabetic() {
        return isDiabetic;
    }

    public void setIsDiabetic(boolean isDiabetic) {
        this.isDiabetic = isDiabetic;
    }

    public boolean isHasBloodPressure() {
        return hasBloodPressure;
    }

    public void setHasBloodPressure(boolean hasBloodPressure) {
        this.hasBloodPressure = hasBloodPressure;
    }    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Owner
 */
public class Family {
    
    private PersonDirectory personDirectory;
    private static int familyId=0;
    private String alphaId;
    private String familyName;

    
 public Family(String familyName){
     this.familyName = familyName;
     personDirectory = new PersonDirectory();
     familyId++;
     alphaId= "F"+familyId;
 }

    /**
     * @return the familyId
     */
    public int getFamilyId() {
        return familyId;
    }

    /**
     * @param familyId the familyId to set
     */
    public void setFamilyId(int familyId) {
        this.familyId = familyId;
    }

    /**
     * @return the familyName
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * @param familyName the familyName to set
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * @return the personDirectory
     */
    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    /**
     * @param personDirectory the personDirectory to set
     */
    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    /**
     * @return the alphaId
     */
    public String getAlphaId() {
        return alphaId;
    }

    /**
     * @param alphaId the alphaId to set
     */
    public void setAlphaId(String alphaId) {
        this.alphaId = alphaId;
    }
    
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Owner
 */
public class PersonDirectory {
    private ArrayList<Person> personDirectory;
    
    public PersonDirectory(){
        personDirectory  = new ArrayList<>();
    }

    /**
     * @return the personDirectory
     */
    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    /**
     * @param personDirectory the personDirectory to set
     */
    public void setPersonDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }
    
    public Person addPerson(String name, String age, String gender,Person mother,Person father,Person siblings){
        Person person = new Person(name,age,gender,mother,father,siblings);
        personDirectory.add(person);
        return person;
    }
    
    public void removePerson(Person person){
        personDirectory.remove(person);
    }
}

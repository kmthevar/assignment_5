/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Admin
 */
public class City {
    
    private String cityName;
    private int size=1000;
    private CommunityDirectory communityDirectory;
    
    public City(String cityName) {
        this.cityName=cityName;
        communityDirectory = new CommunityDirectory();
    }
    


    public int getSize() {
        return size;
    }

    public CommunityDirectory getCommunityDirectory() {
        return communityDirectory;
    }

    public void setCommunityDirectory(CommunityDirectory communityDirectory) {
        this.communityDirectory = communityDirectory;
    }

    /**
     * @return the cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName the cityName to set
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
     public void getCityMaleDiabetics(){
        int countMale=0;
        int personsMale =0;
        int countFemale = 0;
        int personFemale = 0;
        for (Community community : communityDirectory.getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for(Person person : family.getPersonDirectory().getPersonDirectory()){
                        
                        if(person.getGender()=="male"){
                            personsMale++;
                                VitalSign mean = person.getPatient().getVitalSignHistory().averageVitals(person);
                                if(mean.isIsDiabetic()){
                                    countMale++;
                                }
                                
                            
                            
                        }
                        else {
                            personFemale++;
                                VitalSign mean = person.getPatient().getVitalSignHistory().averageVitals(person);
                                if(mean.isIsDiabetic()){
                                    countFemale++;
                            
                        }
                    }
                }
    }}}System.out.println("Male population: "+ personsMale);
    System.out.println("Female population: "+ personFemale);
    System.out.println("Male populatiion with diabetes: "+ countMale);
    System.out.println("Female populatiion with diabetes: "+ countFemale);
        }   
    
    public void populationCount(){
        int population=0;
        for(Community community:communityDirectory.getCommunityDirectory()){
            for(House house: community.getHouseDirectory().getHouseDirectory()){
                for(Family family:house.getFamilyDirectory().getFamilyDirectory()){
                    for(Person person: family.getPersonDirectory().getPersonDirectory()){
                        population++;
                    }
                }
            }
        }
    
    System.out.println("Total Population: "+ population);
}
    public void peopleWithHeartDisease() {
        int count = 0;
        for (Community community : communityDirectory.getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        int avgRisk = person.getPatient().getAverageRisk();
                        int riskPercent = person.getPatient().getRiskPercent();
                        if (avgRisk < riskPercent) {
                            count++;
                        }
                    }
                }
            }
        }
        System.out.println("People with heart disease are: " + count);
    }

    public void healthyPeoplepercent() {
        float count = 0;
        float totalPeople = 0;
        float percent = 0;
        for (Community community : communityDirectory.getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        totalPeople++;
                        int avgRisk = person.getPatient().getAverageRisk();
                        int riskPercent = person.getPatient().getRiskPercent();
                        if (avgRisk > riskPercent) {
                            count++;
                        }
                    }
                }
            }
        }
        percent = (count / totalPeople) * 100;
        percent = Math.round(percent * 100);
        percent = Math.round(percent);
        percent = percent / 100;
        System.out.println("Healthy people percent: " + percent + "%");
    }

    public void diabeticPeopleAgeGroup() {
        int children = 0;
        int seniors = 0;
        int adults = 0;
        for (Community community : communityDirectory.getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        if (Integer.parseInt(person.getAge()) < 40) {
                            VitalSign vitalSign = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                children++;
                            }
                        }
                        if (Integer.parseInt(person.getAge()) > 40 && Integer.parseInt(person.getAge()) < 60) {
                            VitalSign vitalSign = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                adults++;
                            }
                        }
                        if (Integer.parseInt(person.getAge()) > 60) {
                            VitalSign vitalSign = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                seniors++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Children: " + children + "\nAdults: " + adults + "\nSeniors: " + seniors);
    }

    public void ratioOfSmokers() {
        int smokers = 0;
        int nonSmokers = 0;
        for (Community community : communityDirectory.getCommunityDirectory()) {
            for (House house : community.getHouseDirectory().getHouseDirectory()) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        VitalSign vitalSign = person.getPatient().getVitalSignHistory().averageVitals(person);
                        if (vitalSign.isIsSmoker() == true) {
                            smokers++;
                        } else {
                            nonSmokers++;
                        }
                    }
                }
            }
        }
        System.out.println(smokers + ":" + nonSmokers);
    }

}
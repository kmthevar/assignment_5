/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Admin
 */
public class Community {
    private static int communityId=0;
    private String alphaId;
    private String communityName;
    private HouseDirectory houseDirectory;
    
    public Community(String communityName){
        this.communityName = communityName;
        communityId++;
        alphaId= "C"+communityId;
        houseDirectory = new HouseDirectory();
    }

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

 

    /**
     * @return the houseDirectory
     */
    public HouseDirectory getHouseDirectory() {
        return houseDirectory;
    }

    /**
     * @param houseDirectory the houseDirectory to set
     */
    public void setHouseDirectory(HouseDirectory houseDirectory) {
        this.houseDirectory = houseDirectory;
    }

    /**
     * @return the communityName
     */
    public String getCommunityName() {
        return communityName;
    }

    /**
     * @param communityName the communityName to set
     */
    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    /**
     * @return the alphaId
     */
    public String getAlphaId() {
        return alphaId;
    }

    /**
     * @param alphaId the alphaId to set
     */
    public void setAlphaId(String alphaId) {
        this.alphaId = alphaId;
    }
    
}

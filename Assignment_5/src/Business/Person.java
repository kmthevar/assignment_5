/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Owner
 */
public class Person {
    
    
    private static int personId=0;
    private String alphaId;
    private String name;
    private String age;
    private String gender;
    Person mother;
    Person father;
    Person siblings;
    Patient patient;
    
    public Person(String name, String age, String gender,Person mother,Person father,Person siblings) {
        patient = new Patient();
        personId++;
        alphaId= "P"+personId;
        this.name = name;
        this.age= age;
        this.gender= gender;
        this.mother = mother;
        this.father = father;
        this.siblings = siblings;
    }

 
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getSiblings() {
        return siblings;
    }

    public void setSiblings(Person siblings) {
        this.siblings = siblings;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * @return the personId
     */
    public int getPersonId() {
        return personId;
    }

    /**
     * @param personId the personId to set
     */
    public void setPersonId(int personId) {
        this.personId = personId;
    }

    /**
     * @return the alphaId
     */
    public String getAlphaId() {
        return alphaId;
    }

    /**
     * @param alphaId the alphaId to set
     */
    public void setAlphaId(String alphaId) {
        this.alphaId = alphaId;
    }
    
    
    
    
}

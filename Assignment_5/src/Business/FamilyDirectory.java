/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Owner
 */
public class FamilyDirectory {
    
 private ArrayList<Family> familyDirectory;
 
 public FamilyDirectory(){
     familyDirectory = new ArrayList<>();
 }

    /**
     * @return the familyDirectory
     */
    public ArrayList<Family> getFamilyDirectory() {
        return familyDirectory;
    }

    /**
     * @param familyDirectory the familyDirectory to set
     */
    public void setFamilyDirectory(ArrayList<Family> familyDirectory) {
        this.familyDirectory = familyDirectory;
    }
    
    public Family addFamily(String name){
        Family family = new Family(name);
        familyDirectory.add(family);
        return family;
    }
    
    public void removeFamily(Family family){
        familyDirectory.remove(family);
    }
    
    
    public boolean maxRisk(String id) {
        boolean flag = false;
        int countDia = 0;
        int countNonDia = 0;
        for (Family family : familyDirectory) {

            if (family.getAlphaId().equalsIgnoreCase(id)) {
                for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                    VitalSign vs = person.getPatient().getVitalSignHistory().getVitalSignHistory().get(4);
                    if (vs.isIsDiabetic()) {
                        countDia++;
                    } else {
                        countNonDia++;
                    }

                }
                System.out.println("The ratio of Diabetic to non Diabetic for Family id "+family.getAlphaId()+ " is " +countDia + ":"+ countNonDia);
                flag = true;
            }

        }
        return flag;
    }
    public boolean checkDiabetesHereditary(String id){
        boolean flag =false;
        boolean fatherDiabetic = false;
        
        boolean child = false;
        
        for(Family family : familyDirectory)
        {
            if(family.getAlphaId().equalsIgnoreCase(id))
            {
                flag = true;
                for(Person person : family.getPersonDirectory().getPersonDirectory())
                {
                    if(Integer.parseInt(person.getAge()) < 40 ) 
                    {
                        child = person.getPatient().getVitalSignHistory().getVitalSignHistory().get(4).isIsDiabetic();
                        Person father = person.getFather();

                        
                            if(father!=null)
                            {
                                fatherDiabetic = father.getPatient().getVitalSignHistory().getVitalSignHistory().get(4).isIsDiabetic();
                                if(child == true && fatherDiabetic == true)
                                {
                                    System.out.println("Diabetes is hereditary in this family!");
                                    break;
                                } else {
                                    System.out.println("Diabetes is not hereditary in this family!");
                                    break;
                                }
                            }

                        
                    }
                }
            }
        }
        
        return flag;
    }

    public boolean highRiskPercentPerson(String id) {
        boolean flag = false;
        
        for(Family family : familyDirectory)
        {
            if(family.getAlphaId().equalsIgnoreCase(id))
            {
                flag = true;
                Person highRiskperson = null;
                for(Person person : family.getPersonDirectory().getPersonDirectory())
                {
                    if(family.getPersonDirectory().getPersonDirectory().indexOf(person) == 0)
                    {
                        highRiskperson = person;
                    } 
                    
                    if(person.getPatient().getRiskPercent() > highRiskperson.getPatient().getRiskPercent())
                    {
                       highRiskperson = person;     
                    }
                    
                }
                
                System.out.println("\nPerson with highest risk percent in this family is: "
                +highRiskperson.getName()+" "+highRiskperson.getGender()+" "+highRiskperson.getAge()+" "
                +highRiskperson.getPatient().getRiskPercent()+"%");
            }
        }
        
        return flag;
    }

}
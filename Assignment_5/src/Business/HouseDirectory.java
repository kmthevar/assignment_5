/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class HouseDirectory {

    private ArrayList<House> houseDirectory;

    public HouseDirectory() {
        houseDirectory = new ArrayList<>();
    }

    /**
     * @return the houseDirectory
     */
    public ArrayList<House> getHouseDirectory() {
        return houseDirectory;
    }

    /**
     * @param houseDirectory the houseDirectory to set
     */
    public void setHouseDirectory(ArrayList<House> houseDirectory) {
        this.houseDirectory = houseDirectory;
    }

    public House addHouse(String name) {
        House house = new House(name);
        houseDirectory.add(house);
        return house;
    }

    public void removeHouse(House house) {
        houseDirectory.remove(house);
    }

    public boolean avgPeopleChart(String alphaId) {
        float DiabeticCount = 0;
        float CholesterolCount = 0;
        boolean flag = false;
        float totalCount = 0;
        for (House house : houseDirectory) {
            if (house.getAlphaId().equalsIgnoreCase(alphaId)) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        totalCount++;
                        VitalSign vs = person.getPatient().getVitalSignHistory().getVitalSignHistory().get(4);
                        if (vs.isIsDiabetic()) {
                            DiabeticCount++;
                        }
                        VitalSign vs1 = person.getPatient().getVitalSignHistory().averageVitals(person);
                        if (vs1.getTotalCholesterol() > 6.22) {
                            CholesterolCount++;
                        }

                        flag = true;
                    }
                }
            }
        }
        if (flag == true) {
            System.out.println("Percent of Diabetic people in House : " + alphaId + " is : " + (DiabeticCount / totalCount)*100 +"%");
            System.out.println("Percent of people with High Cholesterol in House : " + alphaId + " is : " + (CholesterolCount / totalCount)*100 +"%");
        }
        return flag;
    }

    public boolean familyCheck(String alphaId) {
        float personAtRisk = 0;
        float totalCount = 0;
        float riskFamily = 0;
        boolean flag = false;
        Family family1=null;
        for (House house : houseDirectory) {
            if (house.getAlphaId().equalsIgnoreCase(alphaId)) {
                for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                    for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                        totalCount++;
                        if (person.getPatient().getRiskPercent() > person.getPatient().getAverageRisk()) {
                            personAtRisk++;
                        }
                    }
                    if (riskFamily < (personAtRisk / totalCount)) {
                        riskFamily = personAtRisk / totalCount;
                        personAtRisk = 0;
                        totalCount = 0;
                        family1 = family;
                        
                    }
                    
                    flag = true;
                }

            }
        }
        if (flag == true) {
            System.out.println("Family with high risk in the House : " + alphaId + " is : " + family1.getFamilyName());
        }
        return flag;
    }

}

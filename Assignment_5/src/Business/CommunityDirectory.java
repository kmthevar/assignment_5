/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class CommunityDirectory {
    
    private ArrayList<Community> communityDirectory;
    
    public CommunityDirectory() {
        communityDirectory = new ArrayList<>();
    }

    public ArrayList<Community> getCommunityDirectory() {
        return communityDirectory;
    }

    public void setCommunityDirectory(ArrayList<Community> communityDirectory) {
        this.communityDirectory = communityDirectory;
    }
 
    public Community addCommunity(String name) {
        Community community = new Community(name);
        communityDirectory.add(community);
        return community;
    }
    
    public void removeCommunity(Community community) {
        communityDirectory.remove(community);
        
    }
    
    public void hasHighBp(String id) {
        for (Community community : communityDirectory) {
            if (community.getAlphaId().equalsIgnoreCase(id)) {
                int count = 0;
                int total = 0;
                for (House house : community.getHouseDirectory().getHouseDirectory()) {
                    for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                        for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                            total++;
                            VitalSign vs = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (vs.isHasBloodPressure() == true) {
                                count++;
                            }
                        }
                    }
                }
                System.out.println("No of people in community " + community.getCommunityName() + " suffering from high Bp: " + count + "/" + total + "\n\n");
            }
        }
    }

    public void hasHighCholestrol(String id) {
        for (Community community : communityDirectory) {
            if (community.getAlphaId().equalsIgnoreCase(id)) {
                float men = 0;
                int mentotal = 0;
                float women = 0;
                int womentotal = 0;
                for (House house : community.getHouseDirectory().getHouseDirectory()) {
                    for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                        for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                            VitalSign vs = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (person.getGender().equalsIgnoreCase("male")) {
                                mentotal++;
                                if (vs.getTotalCholesterol() > 6.22) {
                                    men++;
                                }
                            } else {
                                womentotal++;
                                if (vs.getTotalCholesterol() > 6.22) {
                                    women++;
                                }
                            }
                        }
                    }
                }
                men = (men / mentotal) * 100;
                women = (women / womentotal) * 100;
                System.out.println("Percent men suffering from High Cholestrol: " + men + "%\n Percent women suffering from High Cholestrol: " + women + "%\n\n");
            }
        }
    }

    public void ageSmokerGroup(String id) {
        for (Community community : communityDirectory) {
            if (community.getAlphaId().equalsIgnoreCase(id)) {
                int young = 0;
                int parent = 0;
                int elder = 0;
                for (House house : community.getHouseDirectory().getHouseDirectory()) {
                    for (Family family : house.getFamilyDirectory().getFamilyDirectory()) {
                        for (Person person : family.getPersonDirectory().getPersonDirectory()) {
                            VitalSign vs = person.getPatient().getVitalSignHistory().averageVitals(person);
                            if (vs.isIsSmoker() == true) {
                                if (Integer.parseInt(person.getAge()) >= 31 && Integer.parseInt(person.getAge()) <= 40) {
                                    young++;
                                } else if (Integer.parseInt(person.getAge()) >= 41 && Integer.parseInt(person.getAge()) <= 60) {
                                    parent++;
                                } else {
                                    elder++;
                                }
                            }
                        }
                    }
                }
                if (young < parent) {
                    if (parent < elder) {
                        System.out.println("Age group with max. No. of Smokers is (61-100) with " + elder + " smokers\n\n");
                    } else {
                        System.out.println("Age group with max. No. of Smokers is (41-60) with " + parent + " smokers\n\n");
                    }
                } else if (young < elder) {
                    if (elder < parent) {
                        System.out.println("Age group with max. No. of Smokers is (41-60) with " + parent + " smokers\n\n");
                    } else {
                        System.out.println("Age group with max. No. of Smokers is (61-100) with " + elder + " smokers\n\n");
                    }
                } else {
                    System.out.println("Age group with max. No. of Smokers is (31-40) with " + young + " smokers\n\n");
                }
            }
        }
    }
    
}
